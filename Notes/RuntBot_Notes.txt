RuntBot

Sight range exposed in RuntBot_Controller; use sight range and lose sight range

2 states
1. Move into range
2. Shooting

Move towards player until
	Can see player and within given distance
When shooting
	Shoot projectiles every 3-4 seconds (exposed)
	Transition to move when lose sight of player or when player too far away
	
