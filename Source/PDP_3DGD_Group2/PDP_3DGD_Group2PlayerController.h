// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

class APDP_3DGD_Group2Character;

#include "PDP_3DGD_Group2PlayerController.generated.h"

UCLASS()
class APDP_3DGD_Group2PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	APDP_3DGD_Group2PlayerController();

	virtual void BeginPlay() override;
protected:
	// Input latches
	float leftIntensity = 0.f, upIntensity = 0.f;
	void LeftAxisInput(float value);
	void UpAxisInput(float value);


	void FirePressed();
	void FireReleased();
	void ShotgunSwitch();
	void MachineGunSwitch();

	APDP_3DGD_Group2Character* myPlayer;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

};


