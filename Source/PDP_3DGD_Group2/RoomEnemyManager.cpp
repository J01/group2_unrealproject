// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomEnemyManager.h"
#include "PDP_3DGD_Group2GameMode.h"
#include "EnemySpawnerBase.h"

// Sets default values for this component's properties
URoomEnemyManager::URoomEnemyManager()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void URoomEnemyManager::RegisterSpawnersInThisRoom(const TArray<UEnemySpawnerBase*>& inRoomSpawners)
{
	spawnersInThisRoom = inRoomSpawners;
	for (UEnemySpawnerBase* spawner : spawnersInThisRoom)
	{
		spawner->SetManager(this);
	}
}
void URoomEnemyManager::RegisterEnemySpawned(AEnemyCharacter* enemy)
{
	enemiesInThisRoom.Add(enemy);
}
void URoomEnemyManager::RegisterEnemyDeath(AEnemyCharacter* const dyingEnemy)
{
	UE_LOG(LogTemp, Display, TEXT("Enemy dying, manager being made aware"));
	
	enemiesInThisRoom.Remove(dyingEnemy);
	if (enemiesInThisRoom.Num() == 0)
	{
		UE_LOG(LogTemp, Display, TEXT("Game mode informed of level complete"));
		gameModePtr->AllEnemiesDefeatedInLevel();
	}
}

void URoomEnemyManager::TriggerSpawns()
{
	for (UEnemySpawnerBase* spawner : spawnersInThisRoom)
	{
		spawner->TriggerSpawn();
	}

	if (spawnersInThisRoom.Num() == 0)
		UE_LOG(LogTemp, Warning, TEXT("Tried to spawn 0 enemies"));
}

// Called when the game starts
void URoomEnemyManager::BeginPlay()
{
	Super::BeginPlay();

	AGameModeBase* gmode = GetWorld()->GetAuthGameMode();
	if (!gmode)
	{
		UE_LOG(LogTemp, Warning, TEXT("Enemy manager could not find the game mode"));
		return;
	}
	
	gameModePtr = Cast<APDP_3DGD_Group2GameMode>(gmode);
	if (!gameModePtr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Enemy manager could not succesfully cast the game mode"));
		return;
	}
}


// Called every frame
void URoomEnemyManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

