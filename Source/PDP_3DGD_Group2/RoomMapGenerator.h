// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RoomSequence.h"

class ARoomBasicFloorAndWalls;


class PDP_3DGD_GROUP2_API RoomMapGenerator
{
public:
	RoomMapGenerator();
	~RoomMapGenerator();

	// Expose some generation properties to the editor; these are indicators, not rules
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation Rules")
		int desiredMainPathLength;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation Rules")
		int maxDeadEndLength;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation Rules")
		bool offShootCanReconnect;

	// Returns true if a valid set of rooms was generated
	bool GenerateRoomGraph(int numRooms);

	UFUNCTION(BlueprintCallable)
	RoomSequence* GetSequence();

private:
	TArray<UBlueprint*> blueprintRooms;

	RoomSequence roomSeq;

	static const int MIN_NUM_ROOMS;

	// Helper functions to get the room classes from blueprints
	inline ARoomBasicFloorAndWalls* GetRoom(int idx) const;
	inline ARoomBasicFloorAndWalls* GetRoom(UBlueprint* bp) const;
};
