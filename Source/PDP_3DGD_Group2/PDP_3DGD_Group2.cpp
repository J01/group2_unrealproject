// Copyright Epic Games, Inc. All Rights Reserved.

#include "PDP_3DGD_Group2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PDP_3DGD_Group2, "PDP_3DGD_Group2" );

DEFINE_LOG_CATEGORY(LogPDP_3DGD_Group2)
 