// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerSpawnComponent.h"
#include "PlayerSpawnGroupComponent.h"

// Sets default values for this component's properties
UPlayerSpawnComponent::UPlayerSpawnComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	UE_LOG(LogTemp, Warning, TEXT("In player spawn component constructor"));
}


// Called when the game starts
void UPlayerSpawnComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlayerSpawnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

