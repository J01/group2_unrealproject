// Copyright Epic Games, Inc. All Rights Reserved.

#include "PDP_3DGD_Group2Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "CharacterWeapon.h"

APDP_3DGD_Group2Character::APDP_3DGD_Group2Character()
{
	UE_LOG(LogTemp, Display, TEXT("In character creation"));

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	UE_LOG(LogTemp, Display, TEXT("Setting up character movement"));
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;


	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Group2Content/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;


	UE_LOG(LogTemp, Display, TEXT("Character creation has finished"));
}

void APDP_3DGD_Group2Character::SwitchToShotgun()
{
	const int shotgunIdx = 0;
	if (currWeaponIdx != shotgunIdx)
	{
		ownedWeaponComponents[currWeaponIdx]->SetHiddenInGame(true, true);
		ownedWeaponComponents[currWeaponIdx]->FireReleased();
		currWeaponIdx = shotgunIdx;
		ownedWeaponComponents[shotgunIdx]->SetHiddenInGame(false, true);
		SwitchToShotgunEffect();
	}
}
void APDP_3DGD_Group2Character::SwitchToMachineGun()
{
	const int machineGunIdx = 1;
	if (currWeaponIdx != machineGunIdx)
	{
		ownedWeaponComponents[currWeaponIdx]->SetHiddenInGame(true, true);
		ownedWeaponComponents[currWeaponIdx]->FireReleased();
		currWeaponIdx = machineGunIdx;
		ownedWeaponComponents[machineGunIdx]->SetHiddenInGame(false, true);
		SwitchToMachineGunEffect();
	}
}


void APDP_3DGD_Group2Character::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		// Get the controller of this player
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;												// trace the mouse to the world, finding the TraceHitResult 
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);

			FVector CursorFV = TraceHitResult.ImpactNormal;							// find the normal to the hit, and find the rotation of that to apply to the decal
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);				// set the world location and rotation
			CursorToWorld->SetWorldRotation(CursorR);

			// set the rotation of the character such that it faces the decal
			FVector charToDecal = TraceHitResult.Location - GetCharacterMovement()->GetActorLocation();
			FRotator charRotation = charToDecal.Rotation();
			// remove non z rotation
			charRotation.SetComponentForAxis(EAxis::Type::X, 0);
			charRotation.SetComponentForAxis(EAxis::Type::Y, 0);

			SetActorRotation(charRotation, ETeleportType::None);
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("No cursor to world"));

}

void APDP_3DGD_Group2Character::PostInitProperties()
{
	Super::PostInitProperties();

	UCharacterMovementComponent* charMove = GetCharacterMovement();
	if (charMove)
	{
		charMove->MaxAcceleration = maxAccelerationProp_;
		charMove->BrakingFriction = brakingFrictionProp_;
		charMove->BrakingDecelerationWalking = brakingDecelationProp_;
		charMove->MaxWalkSpeed  = maximumSpeedProp_;
	}
	else
		UE_LOG(LogInit, Warning, TEXT("Character move component was not found when posting init properties"));

}
void APDP_3DGD_Group2Character::BeginPlay()
{
	Super::BeginPlay();
	// Get all the weapon components on this
	TArray<UCharacterWeapon*> potentialWeapons;
	GetComponents<UCharacterWeapon>(potentialWeapons, true);

	ownedWeaponComponents = potentialWeapons;
	if (ownedWeaponComponents.Num() != 2)
		UE_LOG(LogTemp, Fatal, TEXT("Player must have two weapons"));

	for (size_t i = 0; i < ownedWeaponComponents.Num(); ++i)
	{
		UE_LOG(LogTemp, Display, TEXT("Found weapon %i is %s"), i, *ownedWeaponComponents[i]->GetName());
		if (i != 0)
			ownedWeaponComponents[i]->SetHiddenInGame(true, true);
	}

	UE_LOG(LogTemp, Warning, TEXT("Found %i component weapon objects"), ownedWeaponComponents.Num());
}
