// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerSpawnGroupComponent.h"
#include "PlayerSpawnComponent.h"
#include "RoomBasicFloorAndWalls.h"

UPlayerSpawnGroupComponent::UPlayerSpawnGroupComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	UE_LOG(LogTemp, Warning, TEXT("In spawn group constructor with %i references"), spawnLocations.Num());
}

void UPlayerSpawnGroupComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("In spawn group begin play with %i references"), spawnLocations.Num());
}

int UPlayerSpawnGroupComponent::GetSpawnLocationCount() const
{
	return spawnLocations.Num();
}
class UPlayerSpawnComponent* UPlayerSpawnGroupComponent::GetSpawnPoint(int index) const
{
	if (index < spawnLocations.Num())
		return spawnLocations[index];
	else
	{
		UE_LOG(LogTemp, Fatal, TEXT("Attempting to access a non-existent spawn point"));
		return nullptr;
	}

}
class UPlayerSpawnComponent* UPlayerSpawnGroupComponent::GetRandomSpawnPoint() const
{
	if (spawnLocations.Num() == 0)
	{
		UE_LOG(LogTemp, Fatal, TEXT("Object has no spawn points"));
		return nullptr;
	}
	UE_LOG(LogTemp, Warning, TEXT("There are %i spawners"), spawnLocations.Num());
	int randIdx = FMath::RandRange(0, spawnLocations.Num() - 1);
	return spawnLocations[randIdx];
}
void UPlayerSpawnGroupComponent::AddSpawnPoint(class UPlayerSpawnComponent* spawnPoint)
{
	UE_LOG(LogTemp, Warning, TEXT("Spawn point being added here to a set of %i"), spawnLocations.Num());
	spawnLocations.Add(spawnPoint);
}

void UPlayerSpawnGroupComponent::PostInitProperties()
{
	Super::PostInitProperties();
}

// Called every frame
void UPlayerSpawnGroupComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

