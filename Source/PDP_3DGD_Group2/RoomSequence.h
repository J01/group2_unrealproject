// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class ARoomBasicFloorAndWalls;

/**
 * 
 */
class PDP_3DGD_GROUP2_API RoomSequence
{
public:
	struct RoomNode
	{
		TSubclassOf<ARoomBasicFloorAndWalls> roomClass = nullptr;
		UBlueprint* roomBlueprint = nullptr;

		AActor* thisRoom;

		FVector location;
	};

	// add a room to the sequence
	void AddRoom(TSubclassOf<ARoomBasicFloorAndWalls>, UBlueprint*);
	UFUNCTION(BlueprintCallable, Category="Room Sequence")
	void PrintRoomData() const;


	void SpawnRooms(UWorld* worldToSpawnIn);

	UFUNCTION(BlueprintCallable, Category = "Room Sequence")
	const RoomNode* GetCurrentRoom() const;
	UFUNCTION(BlueprintCallable, Category = "Room Sequence")
	void AdvanceSequence();

	UFUNCTION(BlueprintCallable, Category = "Room Sequence")
	bool IsSequenceDone() { return sequenceIsDone; };

private:
	int currentRoom = 0;
	TArray<RoomNode> rooms;

	bool sequenceIsDone = false;

	FVector basicPosition = FVector::ZeroVector;
	float positionIncrease = 3000.f;
};
