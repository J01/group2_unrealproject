// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "PlayerSpawnGroupComponent.generated.h"

// 
// A component of an actor that handles a group of player spawn points
//

class UPlayerSpawnComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API UPlayerSpawnGroupComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerSpawnGroupComponent();

	// Get the count of the spawn points
	UFUNCTION(BlueprintCallable, Category = "Spawn Collection")
		int GetSpawnLocationCount() const;
	// Get a spawn point at a given index
	UFUNCTION(BlueprintCallable, Category = "Spawn Collection")
		class UPlayerSpawnComponent* GetSpawnPoint(int index) const;
	// Get a random spawn point
	UFUNCTION(BlueprintCallable, Category = "Spawn Collection")
		class UPlayerSpawnComponent* GetRandomSpawnPoint() const;
	UFUNCTION(BlueprintCallable, Category = "Spawn Collection")
		void AddSpawnPoint(class UPlayerSpawnComponent* spawnPoint);
	UFUNCTION(BlueprintCallable, Category = "Spawn Collection")
		const TArray<UPlayerSpawnComponent*>& GetSpawns()
	{
		return spawnLocations;
	}

	UFUNCTION()
	virtual void PostInitProperties() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Vector of the components owned by this 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Points")
		TArray<UPlayerSpawnComponent*> spawnLocations;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
