// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomMapGenerator.h"
#include "AssetRegistry/AssetRegistryModule.h"

#include "RoomBasicFloorAndWalls.h"             // need this as it is what all rooms are derived from
#include "Engine/World.h"
#include "Engine/ObjectLibrary.h"

const int RoomMapGenerator::MIN_NUM_ROOMS = 5;




RoomMapGenerator::RoomMapGenerator()
{
 }
RoomMapGenerator::~RoomMapGenerator()
{
}

bool RoomMapGenerator::GenerateRoomGraph(int numRooms)
{
    if (numRooms < MIN_NUM_ROOMS)
    {
        UE_LOG(LogTemp, Warning, TEXT("Too few rooms in request"));
        return false;
    }

    // Find all room blueprint classes using the asset registry
    // Load and retrieve the whole asset registry
    blueprintRooms.Empty();

    FAssetRegistryModule& assetRegModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule&>(FName("AssetRegistry"));
    IAssetRegistry& assetReg = assetRegModule.Get();

    // Need to use dynamic array even though we're only scanning one path
    TArray<FString> bprintAssetPath;
    bprintAssetPath.Add(TEXT("/Game/Content/Group2Content/Blueprints/Rooms"));
    // Scan a given path for assets; do this to ensure it is all loaded
    assetReg.ScanPathsSynchronous(bprintAssetPath, false);

    // Get the name of the static base class
    FName roomClassName = FName(ARoomBasicFloorAndWalls::StaticClass()->GetName());

    TSet<FName> derivedClassNames;
    {
        // need to have parent and excluded names for get function to run
        TArray<FName> basicNames;
        TSet<FName> excludedNames;
        basicNames.Add(roomClassName);

        assetReg.GetDerivedClassNames(basicNames, excludedNames, derivedClassNames);
    }

    for (FName& name : derivedClassNames)
        UE_LOG(LogTemp, Warning, TEXT("Derived class with name %s found"), *name.ToString());



    // Get all the blueprints that are contained entirely in /Game/; this will include all our rooms
    FARFilter filter;
    filter.bRecursiveClasses = true;
    filter.bRecursivePaths = true;
    filter.ClassNames.Add(UBlueprint::StaticClass()->GetFName());
    filter.PackagePaths.Add("/Game/");

    TArray<FAssetData> assetList;
    assetReg.GetAssets(filter, assetList);
    UE_LOG(LogTemp, Display, TEXT("Number assets found : %i"), assetList.Num());
    for (FAssetData& ad : assetList)
        UE_LOG(LogTemp, Display, TEXT("Asset found : %s"), *ad.AssetName.ToString());


    TArray<FAssetData> subclasses;
    // for every retrieved asset we need to see if we want to keep it
    for (const FAssetData& ad : assetList)
    {
        const FString* genClassPathPtr = ad.TagsAndValues.Find(TEXT("GeneratedClass"));
        if (genClassPathPtr)
        {
            // get just the name part of the path
            const FString classObjectPath = FPackageName::ExportTextPathToObjectPath(*genClassPathPtr);
            const FString className = FPackageName::ObjectPathToObjectName(classObjectPath);

            // see if this is in the derived set
            if (derivedClassNames.Contains(*className))
            {

                // add to subclasses array
                UE_LOG(LogTemp, Display, TEXT("Asset %s passed the vibe check; its a room B)"), *className);

                subclasses.Add(ad);
            }
        }
        else
            UE_LOG(LogTemp, Warning, TEXT("Asset %s had no GenClass pointer"), *ad.AssetName.ToString());
    }


    // lets create one of every room just for fun
    // convert everything to a TArray of blueprints
    for (FAssetData& ad : subclasses)
    {
        UObject* LoadedAsset = ad.GetAsset();
        UBlueprint* CastedBP = Cast<UBlueprint>(LoadedAsset);

        // check that it meets the requirements
        if (CastedBP && CastedBP->GeneratedClass->IsChildOf(ARoomBasicFloorAndWalls::StaticClass()))
        {
            UClass* RoomClassData = *CastedBP->GeneratedClass;

            // Log that we found it
            TSubclassOf<ARoomBasicFloorAndWalls> thisRoom = TSubclassOf<ARoomBasicFloorAndWalls>(CastedBP->GeneratedClass);
            if (thisRoom)
            {
                UE_LOG(LogTemp, Display, TEXT("Room of type %s added to the available pool"), *thisRoom->GetFName().ToString());
                blueprintRooms.Add(CastedBP);
            }
        }
    }

    if (blueprintRooms.Num() <= 0)
    {
        UE_LOG(LogTemp, Warning, TEXT("No rooms present when building sequence"));
        return false;
    }

    // check there is a room with a sufficient max place
    size_t bpIdx = 0;
    while (GetRoom(blueprintRooms[bpIdx])->maxPlaceInSequence < numRooms && bpIdx < blueprintRooms.Num())
        ++bpIdx;
    if (bpIdx == blueprintRooms.Num())
    {
        UE_LOG(LogTemp, Warning, TEXT("No room could meet the maximum number"));
        return false;
    }
    
    UE_LOG(LogTemp, Display, TEXT("Finished checking for things"));


    for (UBlueprint* bp : blueprintRooms)
    {
        ARoomBasicFloorAndWalls* rm = GetRoom(bp);
        UE_LOG(LogTemp, Warning, TEXT("Room with min %i max %i"), rm->minPlaceInSequence, rm->maxPlaceInSequence);
    }

    // get a random blueprint
    for (int i = 0; i < numRooms; ++i)
    {
        UE_LOG(LogTemp, Warning, TEXT("This would be a generation"));
        bool genSuccess = false;
        do
        {
            int randIdx = FMath::RandRange(0, blueprintRooms.Num() - 1);
            ARoomBasicFloorAndWalls* rm = GetRoom(randIdx);
            if (rm->minPlaceInSequence <= i + 1 && rm->maxPlaceInSequence >= i + 1)
            {
                // add the class reference and blueprint to the node
                roomSeq.AddRoom(TSubclassOf<ARoomBasicFloorAndWalls>(rm->GetClass()), blueprintRooms[randIdx]);
                genSuccess = true;
                UE_LOG(LogTemp, Warning, TEXT("Has generated here"));
            }
        } 
        while (!genSuccess);
    }

    roomSeq.PrintRoomData();
    return true;
}
RoomSequence* RoomMapGenerator::GetSequence()
{
    return &roomSeq;
}


inline ARoomBasicFloorAndWalls* RoomMapGenerator::GetRoom(UBlueprint* bp) const
{
    TSubclassOf<ARoomBasicFloorAndWalls> room = TSubclassOf<ARoomBasicFloorAndWalls>(bp->GeneratedClass);
    if (!room)
    {
        UE_LOG(LogTemp, Warning, TEXT("Casted invalid object to room ; failed!"));
        return nullptr;
    }
    return room.GetDefaultObject();
}
inline ARoomBasicFloorAndWalls* RoomMapGenerator::GetRoom(int idx) const
{
    if (idx < 0 || idx >= blueprintRooms.Num())
    {
        UE_LOG(LogTemp, Warning, TEXT("Referencing non existent blueprint"));
        return nullptr;
    }
    return GetRoom(blueprintRooms[idx]);
}
