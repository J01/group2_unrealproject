// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterWeapon.h"

#include "BulletObject.h"

// Sets default values for this component's properties
UCharacterWeapon::UCharacterWeapon()
{
	PrimaryComponentTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	barrelPointAccurate = CreateDefaultSubobject<USceneComponent>(TEXT("BarrelPoint"));
}


// Called when the game starts
void UCharacterWeapon::BeginPlay()
{
	Super::BeginPlay();
	myWorld = GetWorld();
	staticMesh->AttachTo(this);
	barrelPointAccurate->AttachTo(this);
}

void UCharacterWeapon::FirePressed()
{
	isTriggerDown = true;
	isTriggerNewlyDown = true;
}
void UCharacterWeapon::FireReleased()
{
	isTriggerDown = false;
}

void UCharacterWeapon::Fire_Implementation()
{
	UE_LOG(LogTemp, Display, TEXT("Weapon is firing now! Bang!"));
	secondsSinceFire = 0.f;

	// Spawn all the bullets used
	if (myWorld)
		UE_LOG(LogTemp, Display, TEXT("Also has a world pointer so, no excuses are there really"));

	for (size_t i = 0; i < bulletsToCreate.Num(); ++i)
	{
		AActor* spawnedActor = myWorld->SpawnActor<AActor>(bulletsToCreate[i].GetDefaultObject()->GetClass(), barrelPointAccurate->GetComponentTransform());
		if (!spawnedActor)
			UE_LOG(LogTemp, Warning, TEXT("Bullets %i creation failed"), i);
	}
}

void UCharacterWeapon::PostInitProperties()
{
	Super::PostInitProperties();
}

// Called every frame
void UCharacterWeapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// common
	secondsSinceFire += DeltaTime;

	// split based on automatic or not
	if (isAutomatic)
	{
		if (isTriggerDown && secondsSinceFire >= secondsBetweenShots)
		{
			Fire();
		}
	}
	else
	{
		if (isTriggerNewlyDown && secondsSinceFire >= secondsBetweenShots)
		{
			Fire();
		}
	}

	isTriggerNewlyDown = false;
}

