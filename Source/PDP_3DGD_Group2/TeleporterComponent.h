// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "TeleporterComponent.generated.h"

// A component that should be added to a room to act as a means to advance to the next room
class APDP_3DGD_Group2GameMode;
class UBoxComponent;

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API UTeleporterComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTeleporterComponent();

	// functions to activate and deactivate this 
	void TurnOnTeleporter();
	void TurnOffTeleporter();

	UFUNCTION()
		void TeleporterBeginOverlap(UPrimitiveComponent* overlapComp, AActor* otherAct, UPrimitiveComponent* otherComp, int32 otherBodyIdx, bool fromSweep, const FHitResult& sweepResult);
#if WITH_EDITOR
		virtual void PostEditChangeProperty(FPropertyChangedEvent&) override;
#endif 

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Collider")
	UBoxComponent* collider;
protected:

	// Called when the game starts ; needs to find the game mode that is being used, should not be too hard
	virtual void BeginPlay() override;

	APDP_3DGD_Group2GameMode* gameModePtr;
	APawn* playerPawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TeleportActor")
		TSubclassOf<AActor> teleportActorToSpawn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TeleportActor")
		FTransform actorTransform;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TeleportActor")
	AActor* teleportActor;

	UFUNCTION(BlueprintNativeEvent, Category = "Teleporting")
		void TeleportEffect();
	UFUNCTION(BlueprintNativeEvent, Category = "Teleporting")
		void TeleportShown();

	UFUNCTION(BlueprintCallable)
	void TeleportEffect_Implementation() {};
	UFUNCTION(BlueprintCallable)
	void TeleportShown_Implementation() {};

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
