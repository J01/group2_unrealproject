// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "CharacterWeapon.h"

#include "PDP_3DGD_Group2Character.generated.h"


UCLASS(Blueprintable)
class APDP_3DGD_Group2Character : public ACharacter
{
	GENERATED_BODY()

public:
	APDP_3DGD_Group2Character();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	// Get the world cursor decal
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	// Expose some character properties here, for easy tweaking
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Configurable Movement")
		float maxAccelerationProp_;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Configurable Movement")
		float brakingFrictionProp_;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Configurable Movement")
		float brakingDecelationProp_;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Configurable Movement")
		float maximumSpeedProp_;

	// Allow editor properties to be set
	void PostInitProperties();
	void BeginPlay();

	UFUNCTION(BlueprintCallable)
		UCharacterWeapon* GetCurrentWeapon()
	{
		if (currWeaponIdx >= 0 && currWeaponIdx < ownedWeaponComponents.Num())
		{
			UE_LOG(LogTemp, Display, TEXT("Returning a valid weapon"));
			return ownedWeaponComponents[currWeaponIdx];
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Attempting to access %i of %i weapons"), currWeaponIdx, ownedWeaponComponents.Num());
			return nullptr;
		}
	}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Damage")
		void TakeDamage(int amount);
	void TakeDamage_Implementation(int amount) {};

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Health")
		void HealFromRoomTransition();
	void HealFromRoomTransition_Implementation() {};

	void SwitchToShotgun();
	void SwitchToMachineGun();

protected:

	TArray<UCharacterWeapon*> ownedWeaponComponents;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Weapons")
		int currWeaponIdx = 0;

	UFUNCTION(BlueprintNativeEvent)
		void SwitchToShotgunEffect();
	void SwitchToShotgunEffect_Implementation() {};
	UFUNCTION(BlueprintNativeEvent)
		void SwitchToMachineGunEffect();
	void SwitchToMachineGunEffect_Implementation() {};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Health")
		int maxHealth;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Health")
	int currentHealth;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Health")
		int toHealEachRoom;

private:
	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
	
};

