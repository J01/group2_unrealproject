// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PDP_3DGD_Group2 : ModuleRules
{
	public PDP_3DGD_Group2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
