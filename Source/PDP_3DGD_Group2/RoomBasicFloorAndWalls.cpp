// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomBasicFloorAndWalls.h"
#include "PlayerSpawnGroupComponent.h"
#include "TeleporterGroup.h"
#include "RoomEnemyManager.h"
#include "EnemySpawnerBase.h"
#include "PlayerSpawnComponent.h"

bool ARoomBasicFloorAndWalls::isFirstGeneration = true;
ARoomBasicFloorAndWalls::FloorAndWallMatCombo ARoomBasicFloorAndWalls::orangeMaterials;
ARoomBasicFloorAndWalls::FloorAndWallMatCombo ARoomBasicFloorAndWalls::greenMaterials;
ARoomBasicFloorAndWalls::FloorAndWallMatCombo ARoomBasicFloorAndWalls::blueMaterials;
ARoomBasicFloorAndWalls::FloorAndWallMatCombo ARoomBasicFloorAndWalls::redMaterials;

// Sets default values
ARoomBasicFloorAndWalls::ARoomBasicFloorAndWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	roomCentre = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = roomCentre;

	// find a reference to the 1mx1m cube static mesh
	const ConstructorHelpers::FObjectFinder<UStaticMesh> oneMCubeMeshAsset(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	UStaticMesh* cubeMesh = oneMCubeMeshAsset.Object;

	// get the starting wall material
	floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorMesh"));
	floor->SetStaticMesh(cubeMesh);

	eastWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("NorthMesh"));
	eastWall->SetStaticMesh(cubeMesh);
	westWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WestWallMesh"));
	westWall->SetStaticMesh(cubeMesh);

	northWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EastWallMesh"));
	northWall->SetStaticMesh(cubeMesh);
	southWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SouthWallMesh"));
	southWall->SetStaticMesh(cubeMesh);

	// try some attaching
	eastWall->AttachTo(RootComponent);
	westWall->AttachTo(RootComponent);
	northWall->AttachTo(RootComponent);
	southWall->AttachTo(RootComponent);
	floor->AttachTo(RootComponent);


	// setup the spawns
	spawnGroupComponent = CreateDefaultSubobject<UPlayerSpawnGroupComponent>(TEXT("SpawnCollection"));
	spawnGroupComponent->AttachTo(RootComponent);

	// Create the teleporter group
	teleportGroup = CreateDefaultSubobject<UTeleporterGroup>(TEXT("TeleporterGroup"));
	if (teleportGroup->FailedGeneration())
	{
		teleportGroup->DestroyComponent();
	}

	//find the enemy manager
	enemyManager = CreateDefaultSubobject<URoomEnemyManager>(TEXT("EnemyManager"));
	if (!enemyManager)
	{
		UE_LOG(LogTemp, Warning, TEXT("Enemy manager in room could not be created"));
	}


	if (ARoomBasicFloorAndWalls::isFirstGeneration)
	{
		// find orange
		ConstructorHelpers::FObjectFinder<UMaterial> orangeWallMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Orange_Room/Orange_Wall/OrangeRoomWall_Material.OrangeRoomWall_Material'"));
		ConstructorHelpers::FObjectFinder<UMaterial> orangeFloorMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Orange_Room/Orange_Floor/OrangeRoomFloor_Material.OrangeRoomFloor_Material'"));

		UE_LOG(LogTemp, Display, TEXT("I have found some orange materials, maybe"));
		if (orangeWallMat.Object && orangeFloorMat.Object)
			UE_LOG(LogTemp, Display, TEXT("Found them succesfully"));
		ARoomBasicFloorAndWalls::orangeMaterials.floorMat = orangeFloorMat.Object;
		ARoomBasicFloorAndWalls::orangeMaterials.wallMat = orangeWallMat.Object;

		// find green
		ConstructorHelpers::FObjectFinder<UMaterial> greenWallMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Green_Room/Green_Wall/GreenRoomWall_Material.GreenRoomWall_Material'"));
		ConstructorHelpers::FObjectFinder<UMaterial> greenFloorMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Green_Room/Green_Floor/GreenRoomFloor_Material.GreenRoomFloor_Material'"));

		UE_LOG(LogTemp, Display, TEXT("I have found some green materials, maybe"));
		if (greenWallMat.Object && greenFloorMat.Object)
			UE_LOG(LogTemp, Display, TEXT("Found them succesfully"));
		ARoomBasicFloorAndWalls::greenMaterials.floorMat = greenFloorMat.Object;
		ARoomBasicFloorAndWalls::greenMaterials.wallMat = greenWallMat.Object;

		// find blue 
		ConstructorHelpers::FObjectFinder<UMaterial> blueWallMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Blue_Room/Blue_wall/BlueRoomWall_Material.BlueRoomWall_Material'"));
		ConstructorHelpers::FObjectFinder<UMaterial> blueFloorMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Blue_Room/Blue_Floor/BlueRoomFloor_Material.BlueRoomFloor_Material'"));

		UE_LOG(LogTemp, Display, TEXT("I have found some blue materials, maybe"));
		if (blueWallMat.Object && blueFloorMat.Object)
			UE_LOG(LogTemp, Display, TEXT("Found them succesfully"));
		ARoomBasicFloorAndWalls::blueMaterials.floorMat = blueFloorMat.Object;
		ARoomBasicFloorAndWalls::blueMaterials.wallMat = blueWallMat.Object;

		// find red
		ConstructorHelpers::FObjectFinder<UMaterial> redWallMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Red_Room/Red_Wall/RedRoomWall_Material.RedRoomWall_Material'"));
		ConstructorHelpers::FObjectFinder<UMaterial> redFloorMat(TEXT("Material'/Game/Group2Content/ArtAssets/RoomAssets/Red_Room/Red_Floor/RedRoomFloor_Material.RedRoomFloor_Material'"));

		UE_LOG(LogTemp, Display, TEXT("I have found some red materials, maybe"));
		if (redWallMat.Object && redFloorMat.Object)
			UE_LOG(LogTemp, Display, TEXT("Found them succesfully"));
		ARoomBasicFloorAndWalls::redMaterials.floorMat = redFloorMat.Object;
		ARoomBasicFloorAndWalls::redMaterials.wallMat = redWallMat.Object;
	}
	ARoomBasicFloorAndWalls::isFirstGeneration = false;


	SetupEditorLoad();
}

void ARoomBasicFloorAndWalls::ActivateAsLevel()
{
	if (enemyManager)
	{
		enemyManager->TriggerSpawns();
		teleportGroup->TurnOffAllTeleporters();
		UE_LOG(LogTemp, Display, TEXT("Turning off all teleporters for level"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No enemy manager exists for this room"));
	}
}
void ARoomBasicFloorAndWalls::WonLevel()
{
	if (teleportGroup)
	{
		teleportGroup->TurnOnAllTeleporters();
		UE_LOG(LogTemp, Display, TEXT("Won level %s, teleporters now active"), *GetName());
	}
}

// Called when the game starts or when spawned
void ARoomBasicFloorAndWalls::BeginPlay()
{
	Super::BeginPlay();
	SetupEnemies();
	FindSpawnPoints();
	RunAssetRandomization();
	UE_LOG(LogTemp, Warning, TEXT("In room begin play"));
}

void ARoomBasicFloorAndWalls::PostInitProperties()
{
	Super::PostInitProperties();
	UE_LOG(LogTemp, Warning, TEXT("Posted init properties"));	
}

void ARoomBasicFloorAndWalls::FindSpawnPoints()
{
	TArray<UPlayerSpawnComponent*> spawns;
	GetComponents<UPlayerSpawnComponent>(spawns, true);
	for (size_t i = 0; i < spawns.Num(); ++i)
	{
		spawnGroupComponent->AddSpawnPoint(spawns[i]);
	}
	if (spawns.Num() == 0)
		UE_LOG(LogTemp, Warning, TEXT("There are no spawners in room %s"), *GetName());
}

#if WITH_EDITOR
void ARoomBasicFloorAndWalls::PostEditChangeProperty(FPropertyChangedEvent& propEvent)
{
	Super::PostEditChangeProperty(propEvent);
	PostInitializeComponents();
	SetupEditorLoad();
	UE_LOG(LogTemp, Warning, TEXT("Changed room properties"));
}
#endif 
void ARoomBasicFloorAndWalls::SetupEditorLoad()
{	
	// setup the position and scale of floor and all walls
	const FVector floorScale(10, 10, 0.1);
	const FVector floorPosition(0, 0, -60);
	floor->SetRelativeLocation(floorPosition);
	floor->SetRelativeScale3D(floorScale);

	const FVector sideWallScale(1, 10, wallHeightScale);
	const FVector eastWallPosition(550, 0, 0);
	eastWall->SetRelativeLocation(eastWallPosition);
	eastWall->SetRelativeScale3D(sideWallScale);

	const FVector westWallPosition(-550, 0, 0);
	westWall->SetRelativeLocation(westWallPosition);
	westWall->SetRelativeScale3D(sideWallScale);

	const FVector upDownWallScale(10, 1, wallHeightScale);
	const FVector northWallPosition(0, 550, 0);
	northWall->SetRelativeLocation(northWallPosition);
	northWall->SetRelativeScale3D(upDownWallScale);

	const FVector southWallPosition(0, -550, 0);
	southWall->SetRelativeLocation(southWallPosition);
	southWall->SetRelativeScale3D(upDownWallScale);


	// Handle override materials
	if (wallOverrideMat)
	{
		eastWall->SetMaterial(0, wallOverrideMat);
		westWall->SetMaterial(0, wallOverrideMat);
		northWall->SetMaterial(0, wallOverrideMat);
		southWall->SetMaterial(0, wallOverrideMat);
	}
	else
	{
		eastWall->SetMaterial(0, nullptr);
		westWall->SetMaterial(0, nullptr);
		northWall->SetMaterial(0, nullptr);
		southWall->SetMaterial(0, nullptr);
	}
	if (floorOverrideMat)
	{
		floor->SetMaterial(0, floorOverrideMat);
	}
	else
	{
		floor->SetMaterial(0, nullptr);
	}
}

void ARoomBasicFloorAndWalls::SetupEnemies()
{
	if (!enemyManager)
	{
		UE_LOG(LogTemp, Warning, TEXT("Attempting to reference non-existent enemy manager"));
		return;
	}

	// find all the enemies and register them with the manager
	TArray<UActorComponent*> enemySpawners;
	TSubclassOf<UActorComponent> spawnerSubclass = UEnemySpawnerBase::StaticClass();
	GetComponents(spawnerSubclass, enemySpawners);

	UE_LOG(LogTemp, Warning, TEXT("Found %i potential spawners in room"), enemySpawners.Num());

	// convert the spawners that can be converted
	TArray<UEnemySpawnerBase*> convertedSpawners;
	for (UActorComponent* actorComp : enemySpawners)
	{
		UEnemySpawnerBase* asSpawner = Cast<UEnemySpawnerBase>(actorComp);
		if (asSpawner)
		{
			convertedSpawners.Add(asSpawner);
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Found %i actual spawners in room"), convertedSpawners.Num());

	// give the enemy manager the spawner locations
	enemyManager->RegisterSpawnersInThisRoom(convertedSpawners);
}

// Called every frame
void ARoomBasicFloorAndWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARoomBasicFloorAndWalls::RunAssetRandomization()
{
	if (!shouldIRandomizeMyMaterials)
		return;

	int randomMaterialSet = 0;
	randomMaterialSet = FMath::RandRange(0, 3);

	switch (randomMaterialSet)
	{
	case 0:
		// blue 
		floor->SetMaterial(0, blueMaterials.floorMat);
		eastWall->SetMaterial(0, blueMaterials.wallMat);
		westWall->SetMaterial(0, blueMaterials.wallMat);
		northWall->SetMaterial(0, blueMaterials.wallMat);
		southWall->SetMaterial(0, blueMaterials.wallMat);

		break;
	case 1:
		// green
		floor->SetMaterial(0, greenMaterials.floorMat);
		eastWall->SetMaterial(0, greenMaterials.wallMat);
		westWall->SetMaterial(0, greenMaterials.wallMat);
		northWall->SetMaterial(0, greenMaterials.wallMat);
		southWall->SetMaterial(0, greenMaterials.wallMat);

		break;
	case 2:
		// orange 
		floor->SetMaterial(0, orangeMaterials.floorMat);
		eastWall->SetMaterial(0, orangeMaterials.wallMat);
		westWall->SetMaterial(0, orangeMaterials.wallMat);
		northWall->SetMaterial(0, orangeMaterials.wallMat);
		southWall->SetMaterial(0, orangeMaterials.wallMat);

		break;
	case 3:
		// red
		floor->SetMaterial(0, redMaterials.floorMat);
		eastWall->SetMaterial(0, redMaterials.wallMat);
		westWall->SetMaterial(0, redMaterials.wallMat);
		northWall->SetMaterial(0, redMaterials.wallMat);
		southWall->SetMaterial(0, redMaterials.wallMat);

		break;
	}
}