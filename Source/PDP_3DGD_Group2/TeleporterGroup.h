// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TeleporterGroup.generated.h"

class ARoomBasicFloorAndWalls;
class UTeleporterComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API UTeleporterGroup : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTeleporterGroup();

	void TurnOnAllTeleporters();
	void TurnOffAllTeleporters();

	bool FailedGeneration()
	{
		return bFailedGeneration;
	}
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	ARoomBasicFloorAndWalls* containingRoom;
	UPROPERTY()
	TArray<UTeleporterComponent*> teleporters;


	bool bFailedGeneration = false;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
