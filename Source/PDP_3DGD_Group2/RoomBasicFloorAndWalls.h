// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

class UPlayerSpawnGroupComponent;
class UTeleporterGroup;
class URoomEnemyManager;

#include "RoomBasicFloorAndWalls.generated.h"


UCLASS()
class PDP_3DGD_GROUP2_API ARoomBasicFloorAndWalls : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoomBasicFloorAndWalls();

	virtual void PostInitProperties() override;
	//virtual void PreInitializeComponents() override;
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& propEvent) override;
#endif 

	// Call when this becomes the active level
	void ActivateAsLevel();
	// Call when all enemies have been defeated
	void WonLevel();


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation", meta = ( ClampMin = 1 ))
		int minPlaceInSequence = 1;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation", meta = ( ClampMin = 1 ))
		int maxPlaceInSequence = 1;


	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Walls")
	USceneComponent* roomCentre;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Walls")
		float wallHeightScale = 10.f;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Walls")
		UMaterial* wallOverrideMat = nullptr;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Walls")
		UMaterial* floorOverrideMat = nullptr;

	const UPlayerSpawnGroupComponent* GetSpawnGroup() const 
	{
		return spawnGroupComponent;
	}
	UTeleporterGroup* GetTeleporterGroup() const
	{
		return teleportGroup;
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// hold the room components
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Walls")
		UStaticMeshComponent* floor;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Walls")
		UStaticMeshComponent* eastWall;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Walls")
		UStaticMeshComponent* westWall;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Walls")
		UStaticMeshComponent* northWall;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Walls")
		UStaticMeshComponent* southWall;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Spawns")
		UPlayerSpawnGroupComponent* spawnGroupComponent;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UTeleporterGroup* teleportGroup;

	void SetupEnemies();
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	URoomEnemyManager* enemyManager;

	void FindSpawnPoints();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Generation")
	bool shouldIRandomizeMyMaterials = true;
	void RunAssetRandomization();

	static bool isFirstGeneration;
	struct FloorAndWallMatCombo
	{
		UMaterial* floorMat;
		UMaterial* wallMat;
	};
	static FloorAndWallMatCombo orangeMaterials;
	static FloorAndWallMatCombo redMaterials;
	static FloorAndWallMatCombo blueMaterials;
	static FloorAndWallMatCombo greenMaterials;
	
private:
	void SetupEditorLoad();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
