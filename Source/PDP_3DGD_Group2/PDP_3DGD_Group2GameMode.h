// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "RoomMapGenerator.h"
#include "Components/TimelineComponent.h"

#include "PDP_3DGD_Group2GameMode.generated.h"

class UTimelineComponent;
class UCurveFloat;
class ANavMeshBoundsVolume;

UCLASS(minimalapi)
class APDP_3DGD_Group2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APDP_3DGD_Group2GameMode();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetupRooms();

	RoomMapGenerator& GetRoomGenerator()
	{
		return rmg;
	}

	void StartNextLevel();
	void AllEnemiesDefeatedInLevel();
	void EndLevel();

	virtual void RestartPlayer(AController* pControl) override;
	virtual void BeginPlay() override;

	virtual void Tick(float delta) override;

	UFUNCTION(BlueprintImplementableEvent)
		void GameWon();
	void GameWon_Implementation();

	UFUNCTION(BlueprintImplementableEvent)
		void TransitionEffect();
	void TransitionEffect_Implementation();

protected:
	void ReSpawnPlayer();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera")
		FVector positionRelativeRoom = FVector::ZeroVector;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera")
		FRotator rotationRelativeWorld = FRotator::ZeroRotator;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Room Pool")
		TArray<TSubclassOf<ARoomBasicFloorAndWalls>> roomPool;

	RoomMapGenerator rmg;
	AController* playerController = nullptr;
	ACameraActor* worldCamera = nullptr;

	ARoomBasicFloorAndWalls* previousRoom = nullptr;
	bool needsToStartFirstRoom = true;

	UPROPERTY()
		AActor* testRoom;

	struct TransitionData
	{
		UTimelineComponent* timeline;
		UCurveFloat* floatCurveToUse;

		FTransform startTransform;
		FTransform endTransform;
	};
	TransitionData levelTransition;

	FOnTimelineFloat timelineUpdateCallback;
	UFUNCTION()
	void UpdateTimelineTransition(float val);

	ANavMeshBoundsVolume* navBounds;
};



