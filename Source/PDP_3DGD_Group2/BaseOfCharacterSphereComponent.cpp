// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseOfCharacterSphereComponent.h"

// Sets default values for this component's properties
UBaseOfCharacterSphereComponent::UBaseOfCharacterSphereComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	staticMeshProperty = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
}


// Called when the game starts
void UBaseOfCharacterSphereComponent::BeginPlay()
{
	Super::BeginPlay();
	staticMeshProperty->AttachTo(this);
}
void UBaseOfCharacterSphereComponent::PostInitProperties()
{
	Super::PostInitProperties();
	SetupEditorProperties();
}
#if WITH_EDITOR
void UBaseOfCharacterSphereComponent::PostEditChangeProperty(FPropertyChangedEvent& propEvent)
{
	Super::PostEditChangeProperty(propEvent);
	SetupEditorProperties();
}
#endif

void UBaseOfCharacterSphereComponent::SetupEditorProperties()
{
	// if there is nothing posted for the 
	if (!staticMeshProperty)
		return;

	bool isAssignedStaticMesh = (staticMeshProperty->GetStaticMesh() != nullptr);
	PrimaryComponentTick.bCanEverTick = isAssignedStaticMesh;		// as this is almost purely cosmetic we can afford to not update when not showing

	// assign the material to the static mesh
	if (overrideMaterial && isAssignedStaticMesh)
	{
		staticMeshProperty->SetMaterial(0, overrideMaterial);
	}

	if (!GetAttachmentRootActor())
	{
		UE_LOG(LogTemp, Warning, TEXT("No root actor for rolling base"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Rolling base attached to actor %s"), *GetAttachmentRootActor()->GetName());
	}
}

// Called every frame
void UBaseOfCharacterSphereComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Lets get a reference to our root actor
	AActor* myActor = GetAttachmentRootActor();
	
	// get the speed of the actor
	if (myActor)
	{
		const FVector currentVelocity = myActor->GetVelocity();
		float currentSpeed = currentVelocity.Size();

		// get component of velocity parallel to the facing direction
		const FVector facingDirection = myActor->GetActorForwardVector();
		const float dotFacingVel = FVector::DotProduct(facingDirection, currentVelocity);
		// this is the speed parallel

		//speed perpendicular 
		if (dotFacingVel > 0)
			currentSpeed *= -1;
		const float nonFacingVel = currentSpeed - dotFacingVel;
	

		// need to rotate around y axis for forward movement
		FRotator toRotate(0, 0, 0);
		toRotate.Pitch = -dotFacingVel * rotationRate;
		toRotate.Roll = nonFacingVel * rotationRate;
		
		AddLocalRotation(toRotate, false);
	}
}

