// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "RoomEnemyManager.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	currentHealth = maxHealth;
}

void AEnemyCharacter::TakeDamage(int amountToTake)
{
	currentHealth -= amountToTake;
	if (currentHealth <= 0)
	{
		Destroy(false, true);
		// can still be referenced for rest of tick, shaky but whatever

		if (myManager)
		{
			myManager->RegisterEnemyDeath(this);
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("DYING ENEMY HAS NO MANAGER"));
	}

	UE_LOG(LogTemp, Warning, TEXT("Enemy %s recieving %i damage leaving %i health"), *GetName(), amountToTake, currentHealth);
}
bool AEnemyCharacter::IsDead()
{
	return (currentHealth <= maxHealth);
}
void AEnemyCharacter::RestoreToMaxHealth()
{
	// only allow healing if not dead
	if (currentHealth > 0)
		currentHealth = maxHealth;
}
int AEnemyCharacter::GetCurrentHealth()
{
	return currentHealth;
}



// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

