// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

class URoomEnemyManager;

UCLASS()
class PDP_3DGD_GROUP2_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	void SetManager(URoomEnemyManager* enemyManager)
	{
		myManager = enemyManager;
	}

	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void TakeDamage(int amountToTake);
	UFUNCTION(BlueprintCallable, Category = "Enemy")
		bool IsDead();
	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void RestoreToMaxHealth();
	UFUNCTION(BlueprintCallable, Category = "Enemy")
		int GetCurrentHealth();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	URoomEnemyManager* myManager = nullptr;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Enemy Stats")
		int maxHealth;
	int currentHealth;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
