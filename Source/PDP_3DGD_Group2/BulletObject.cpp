// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletObject.h"

// Sets default values
ABulletObject::ABulletObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<class UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = staticMesh;

	// lets add a projectile movement
	projMovement = CreateDefaultSubobject<class UProjectileMovementComponent>(TEXT("MovementComp"));
}

// Called when the game starts or when spawned
void ABulletObject::BeginPlay()
{
	Super::BeginPlay();

	// if this has no static mesh, just kill it; it can't do anything
	if (!staticMesh->GetStaticMesh())
		Destroy();
}

// Called every frame
void ABulletObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

