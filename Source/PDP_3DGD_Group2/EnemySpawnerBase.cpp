// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawnerBase.h"
#include "RoomEnemyManager.h"

// Sets default values for this component's properties
UEnemySpawnerBase::UEnemySpawnerBase()
{
	PrimaryComponentTick.bCanEverTick = false;
	UE_LOG(LogTemp, Warning, TEXT("In spawner constructor"));
}


// Called when the game starts
void UEnemySpawnerBase::BeginPlay()
{
	Super::BeginPlay();
}

void UEnemySpawnerBase::TriggerSpawn()
{
	UE_LOG(LogTemp, Warning, TEXT("Spawns triggered"));
	if (!managingComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawner has no pointer to the enemy manager"));
		return;
	}

	AEnemyCharacter* spawnedActor;
	const FTransform& thisTransform = GetComponentTransform();
	spawnedActor = GetWorld()->SpawnActor<AEnemyCharacter>(enemySubclass.GetDefaultObject()->GetClass(), thisTransform);

	if (!spawnedActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawner failed to create an enemy actor!"));
		return;
	}
	else
	{
		spawnedActor->SetManager(managingComponent);
	}
	managingComponent->RegisterEnemySpawned(spawnedActor);

	OnSpawnTrigger();
}


// Called every frame
void UEnemySpawnerBase::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

