// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RoomEnemyManager.generated.h"

class AEnemyCharacter;
class UEnemySpawnerBase;
class APDP_3DGD_Group2GameMode;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API URoomEnemyManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URoomEnemyManager();

	void RegisterSpawnersInThisRoom(const TArray<UEnemySpawnerBase*>& inRoom);
	void RegisterEnemySpawned(AEnemyCharacter* spawned);
	void RegisterEnemyDeath(AEnemyCharacter* const dyingEnemy);

	void TriggerSpawns();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	TArray<AEnemyCharacter*> enemiesInThisRoom;
	TArray<UEnemySpawnerBase*> spawnersInThisRoom;
	APDP_3DGD_Group2GameMode* gameModePtr = nullptr;

	bool managerHasSpawned = false;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
