// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "CharacterWeapon.generated.h"

class ABulletObject;


UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PDP_3DGD_GROUP2_API UCharacterWeapon : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterWeapon();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		TArray<TSubclassOf<ABulletObject>> bulletsToCreate;


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		float secondsBetweenShots = 1.f;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		bool isAutomatic = false;

	UFUNCTION(BlueprintCallable)
	void FirePressed();
	UFUNCTION(BlueprintCallable)
	void FireReleased();

	virtual void PostInitProperties() override;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float secondsSinceFire = 0.f;
	bool  isTriggerDown = false;
	bool  isTriggerNewlyDown = false;

	UWorld* myWorld;

	// Allow fire to be extended, needs a fire implementation call though
	UFUNCTION(BlueprintNativeEvent, Category = "Firing")
		void Fire();
	UFUNCTION(BlueprintCallable)
		void Fire_Implementation();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Mesh")
		UStaticMeshComponent* staticMesh;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Firing")
		USceneComponent* barrelPointAccurate;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
