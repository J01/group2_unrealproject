// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleporterGroup.h"
#include "RoomBasicFloorAndWalls.h"
#include "TeleporterComponent.h"

// Sets default values for this component's properties
UTeleporterGroup::UTeleporterGroup()
{
	PrimaryComponentTick.bCanEverTick = true;

	// Find the parent room
	AActor* parentActor;
	parentActor = GetOwner();
	if (!parentActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("No parent actor, teleporter group failed creation"));
		bFailedGeneration = true;
		return;
	}

	containingRoom = Cast<ARoomBasicFloorAndWalls>(parentActor);
	if (!containingRoom)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not cast owner to room class, failed creation"));
		bFailedGeneration = true;
		return;
	}

}

void UTeleporterGroup::TurnOnAllTeleporters()
{
	UE_LOG(LogTemp, Display, TEXT("Turning on teleporters"));
	for (UTeleporterComponent* teleport : teleporters)
	{
		teleport->TurnOnTeleporter();
	}
}
void UTeleporterGroup::TurnOffAllTeleporters()
{
	UE_LOG(LogTemp, Display, TEXT("Turning off teleporters"));
	for (UTeleporterComponent* teleport : teleporters)
	{
		teleport->TurnOffTeleporter();
	}
}

// Called when the game starts
void UTeleporterGroup::BeginPlay()
{
	Super::BeginPlay();

	// find all the teleporters in the room
	TSubclassOf<UTeleporterComponent> teleporterSubclass = UTeleporterComponent::StaticClass();

	TArray<UActorComponent*> actComps;
	containingRoom->GetComponents(teleporterSubclass, actComps);

	for (UActorComponent* component : actComps)
	{
		UTeleporterComponent* asTeleporter = Cast<UTeleporterComponent>(component);
		if (asTeleporter)
		{
			teleporters.Add(asTeleporter);
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Teleporter group found %i elements"), teleporters.Num());
	TurnOffAllTeleporters();
}


// Called every frame
void UTeleporterGroup::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

