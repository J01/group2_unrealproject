// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "EnemyCharacter.h"
class URoomEnemyManager;

#include "EnemySpawnerBase.generated.h"


UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API UEnemySpawnerBase : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemySpawnerBase();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "To Spawn")
	TSubclassOf<AEnemyCharacter> enemySubclass;

	void TriggerSpawn();
	void SetManager(URoomEnemyManager* myManager) { managingComponent = myManager; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	URoomEnemyManager* managingComponent = nullptr;

	UFUNCTION(BlueprintNativeEvent)
		void OnSpawnTrigger();
	void OnSpawnTrigger_Implementation() {};
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
