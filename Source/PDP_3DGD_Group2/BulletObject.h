// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "GameFramework/ProjectileMovementComponent.h"

#include "BulletObject.generated.h"

UCLASS(Blueprintable, meta = ( BlueprintSpawnable ))
class PDP_3DGD_GROUP2_API ABulletObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABulletObject();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		int damage_;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		float speed_;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Firing")
		float maxLifetime_;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Rendering")
		UStaticMeshComponent* staticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Firing")
		UProjectileMovementComponent* projMovement;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
