// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleporterComponent.h"

#include "Engine/World.h"
#include "GameFramework/GameState.h"
#include "PDP_3DGD_Group2GameMode.h"
#include "RoomMapGenerator.h"

#include "Components/BoxComponent.h"

// Sets default values for this component's properties
UTeleporterComponent::UTeleporterComponent()
{
	PrimaryComponentTick.bCanEverTick = true;


	collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
}

void UTeleporterComponent::TurnOnTeleporter()
{
	SetComponentTickEnabled(true);
	collider->SetGenerateOverlapEvents(true);
	UE_LOG(LogTemp, Warning, TEXT("Teleporter promises it turned on overlaps"));

	if (teleportActor)
		teleportActor->SetActorHiddenInGame(false);
	TeleportShown();
}
void UTeleporterComponent::TurnOffTeleporter()
{
	SetComponentTickEnabled(false);
	collider->SetGenerateOverlapEvents(false);
	UE_LOG(LogTemp, Warning, TEXT("Teleporter turned off overlaps"));

	if (teleportActor)
		teleportActor->SetActorHiddenInGame(true);
}

void UTeleporterComponent::TeleporterBeginOverlap(UPrimitiveComponent* overlapComp, AActor* otherAct, UPrimitiveComponent* otherComp, int32 otherBodyIdx, bool fromSweep, const FHitResult& sweepResult)
{
	if (!otherAct)
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlapping with non actor"));
	}
	else if (otherAct != playerPawn)
	{
		UE_LOG(LogTemp, Display, TEXT("Began overlap with non player actor"));
	}
	else
	{
		UE_LOG(LogTemp, Display, TEXT("From %s %s : "), *GetAttachmentRootActor()->GetName(), *GetName());
		UE_LOG(LogTemp, Display, TEXT("Teleporting player from  %f %f %f"), GetComponentLocation().X, GetComponentLocation().Y, GetComponentLocation().Z);
		UE_LOG(LogTemp, Display, TEXT("Player is at %f %f %f"), playerPawn->GetActorLocation().X, playerPawn->GetActorLocation().Y, playerPawn->GetActorLocation().Z);

		
		// if it overlapped with the player actor, then we need to advance the rooms
		gameModePtr->EndLevel();
		UE_LOG(LogTemp, Warning, TEXT("Triggering the teleport effect"));
		if (teleportActor)
			teleportActor->SetHidden(false);
		TeleportEffect();				// trigger the effect that plays on a teleport
	}
}
#if WITH_EDITOR
void UTeleporterComponent::PostEditChangeProperty(FPropertyChangedEvent& fEditProp)
{
	Super::PostEditChangeProperty(fEditProp);
}
#endif 

// Called when the game starts
void UTeleporterComponent::BeginPlay()
{
	Super::BeginPlay();
	collider->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
	collider->OnComponentBeginOverlap.AddDynamic(this, &UTeleporterComponent::TeleporterBeginOverlap);


	// Gather the game mode
	AGameModeBase* gMode = GetWorld()->GetAuthGameMode();
	if (gMode)
	{
		UE_LOG(LogTemp, Display, TEXT("Teleporter found game mode of name %s"), *gMode->GetFName().ToString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("NO GAME MODE FOUND!"));
	}

	//attempt to cast to real type
	gameModePtr = Cast<APDP_3DGD_Group2GameMode>(gMode);
	if (!gameModePtr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not cast game mode to correct type!"));
	}
	else
	{
		// attempt to find the player from the game mode
		AController* playerCtrlr = GetWorld()->GetFirstPlayerController();
		playerPawn = playerCtrlr->GetPawn();
	}

	if (teleportActorToSpawn)
	{
		teleportActor = GetWorld()->SpawnActor<AActor>(teleportActorToSpawn->GetDefaultObject()->GetClass());
		if (!teleportActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("Teleport actor spawn failed"));
		}
		else
		{
			// hide the teleport actor
			teleportActor->SetActorHiddenInGame(true);
			teleportActor->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);

			UE_LOG(LogTemp, Display, TEXT("The teleport actor was spawned"));
			teleportActor->AddActorLocalTransform(actorTransform);
		}
	}

}


// Called every frame
void UTeleporterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

