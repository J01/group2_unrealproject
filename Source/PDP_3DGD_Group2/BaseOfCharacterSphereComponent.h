// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "BaseOfCharacterSphereComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PDP_3DGD_GROUP2_API UBaseOfCharacterSphereComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseOfCharacterSphereComponent();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Mesh")
		UStaticMeshComponent* staticMeshProperty = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Mesh")
		UMaterial* overrideMaterial = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
		float rotationRate;

	void PostInitProperties();
#if WITH_EDITOR
	void PostEditChangeProperty(FPropertyChangedEvent& propEvent);
#endif 

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	void SetupEditorProperties();

	FVector previousTickVelocityVector_;
	FVector previousTickVelocityRotator_;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
