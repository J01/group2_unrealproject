// Copyright Epic Games, Inc. All Rights Reserved.

#include "PDP_3DGD_Group2PlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "PDP_3DGD_Group2Character.h"

#include <sstream>

#include "Engine/World.h"


APDP_3DGD_Group2PlayerController::APDP_3DGD_Group2PlayerController()
{
	UE_LOG(LogTemp, Display, TEXT("Player controller in creation"));
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void APDP_3DGD_Group2PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	FVector inputDirections(leftIntensity, upIntensity, 0);
	const float inDirectionMag = inputDirections.Size();
	
	if (inDirectionMag > 1)
	{
		inputDirections /= inDirectionMag;
	}

	ACharacter* myCharacter = GetCharacter();
	if (myCharacter)
		myCharacter->AddMovementInput(inputDirections);
}

void APDP_3DGD_Group2PlayerController::BeginPlay()
{
	Super::BeginPlay();
	myPlayer = Cast<APDP_3DGD_Group2Character>(GetCharacter());
}

void APDP_3DGD_Group2PlayerController::LeftAxisInput(float value)
{
	value = FMath::Clamp<float>(value, -1, 1);	
	leftIntensity = value;
}
void APDP_3DGD_Group2PlayerController::UpAxisInput(float value)
{
	value = FMath::Clamp<float>(value, -1, 1);
	upIntensity = value;
}

void APDP_3DGD_Group2PlayerController::FireReleased()
{
	UE_LOG(LogTemp, Warning, TEXT("Fire button has been released"));
	if (myPlayer)
	{
		UCharacterWeapon* currWeapon = myPlayer->GetCurrentWeapon();
		if (currWeapon)
		{
			currWeapon->FireReleased();
		}
	}
}
void APDP_3DGD_Group2PlayerController::FirePressed()
{
	UE_LOG(LogTemp, Warning, TEXT("Fire button has been pressed"));
	if (myPlayer)
	{
		UCharacterWeapon* currWeapon = myPlayer->GetCurrentWeapon();
		if (currWeapon)
		{
			currWeapon->FirePressed();
		}
	}
}
void APDP_3DGD_Group2PlayerController::ShotgunSwitch()
{
	if (myPlayer)
		myPlayer->SwitchToShotgun();
}
void APDP_3DGD_Group2PlayerController::MachineGunSwitch()
{
	if (myPlayer)
		myPlayer->SwitchToMachineGun();
}

void APDP_3DGD_Group2PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &APDP_3DGD_Group2PlayerController::UpAxisInput);
	InputComponent->BindAxis("MoveRight", this, &APDP_3DGD_Group2PlayerController::LeftAxisInput);

	InputComponent->BindAction("PlayerFire", EInputEvent::IE_Pressed, this, &APDP_3DGD_Group2PlayerController::FirePressed);
	InputComponent->BindAction("PlayerFire", EInputEvent::IE_Released, this, &APDP_3DGD_Group2PlayerController::FireReleased);

	InputComponent->BindAction("SetWeaponShotgun", EInputEvent::IE_Pressed, this, &APDP_3DGD_Group2PlayerController::ShotgunSwitch);
	InputComponent->BindAction("SetWeaponMachineGun", EInputEvent::IE_Pressed, this, &APDP_3DGD_Group2PlayerController::MachineGunSwitch);}


