// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomSequence.h"
#include "RoomBasicFloorAndWalls.h"

void RoomSequence::AddRoom(TSubclassOf<ARoomBasicFloorAndWalls> roomClass, UBlueprint* blueprint)
{
    RoomNode node;
    node.roomBlueprint = blueprint;
    node.roomClass = roomClass;
    node.location = basicPosition;
    node.location.X += (rooms.Num() * positionIncrease);

    rooms.Add(node);
}
void RoomSequence::PrintRoomData() const
{
    for (const RoomNode& rm : rooms)
    {
        UE_LOG(LogTemp, Display, TEXT("Room of type %s at %f %f %f"), *rm.roomClass.GetDefaultObject()->GetName(), rm.location.X, rm.location.Y, rm.location.Z);
    }

    if (rooms.Num() == 0)
    {
        UE_LOG(LogTemp, Display, TEXT("No rooms to print"));
    }
}

void RoomSequence::SpawnRooms(UWorld* worldToSpawnIn)
{
    UE_LOG(LogTemp, Warning, TEXT("In spawning place. %i rooms."), rooms.Num());
    for (RoomNode& rm : rooms)
    {
        AActor* outActor = worldToSpawnIn->SpawnActor(rm.roomBlueprint->GeneratedClass.Get(), &rm.location);

        if (outActor)
        {
            rm.thisRoom = outActor;
            UE_LOG(LogTemp, Warning, TEXT("Supposedly spawned a room and moved it to %f %f %f"), rm.location.X, rm.location.Y, rm.location.Z);
            UE_LOG(LogTemp, Warning, TEXT("Actor has position %f %f %f"), outActor->GetActorLocation().X, outActor->GetActorLocation().Y, outActor->GetActorLocation().Z);
        }
        else
            UE_LOG(LogTemp, Warning, TEXT("Room spawning failed"));
    }
}

void RoomSequence::AdvanceSequence()
{
    // check to see if this is the end ; if it is send out a warning but don't advance the sequence
    ++currentRoom;
    if (currentRoom >= rooms.Num())
    {
        --currentRoom;
        UE_LOG(LogTemp, Warning, TEXT("Trying to advance beyond end of sequence"));
        sequenceIsDone = true;
    }
}
const RoomSequence::RoomNode* RoomSequence::GetCurrentRoom() const
{
    if (currentRoom < rooms.Num())
        return &rooms[currentRoom];
    else
    {
        UE_LOG(LogTemp, Fatal, TEXT("Attempting to return non existent entity"));
        return nullptr;
    }
}