// Copyright Epic Games, Inc. All Rights Reserved.

#include "PDP_3DGD_Group2GameMode.h"
#include "PDP_3DGD_Group2PlayerController.h"
#include "PDP_3DGD_Group2Character.h"
#include "UObject/ConstructorHelpers.h"

#include "RoomBasicFloorAndWalls.h"
#include "PlayerSpawnGroupComponent.h"
#include "PlayerSpawnComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Camera/CameraActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "TeleporterGroup.h"
#include "NavMesh/NavMeshBoundsVolume.h"


APDP_3DGD_Group2GameMode::APDP_3DGD_Group2GameMode()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	UE_LOG(LogTemp, Display, TEXT("Game mode being set up"));
	UE_LOG(LogTemp, Display, TEXT("		Player controller being set up"));

	// use our custom PlayerController class
	PlayerControllerClass = APDP_3DGD_Group2PlayerController::StaticClass();


	UE_LOG(LogTemp, Display, TEXT("		Player pawn object being set up"));
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Class'/Game/Group2Content/Blueprints/Character/Group2CharacterTopDownCustom'"));

	// Set this to be the default pawn class; perhaps a little late?
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
		UE_LOG(LogTemp, Display, TEXT("		Default pawn class assigned"));
	}

	// Load the effect curve
	static ConstructorHelpers::FObjectFinder<UCurveFloat> effectCurve(TEXT("CurveFloat'/Game/Group2Content/Blueprints/LevelTransition/LevelTransitionEffectCurve.LevelTransitionEffectCurve'"));
	if (effectCurve.Object)
	{
		levelTransition.floatCurveToUse = effectCurve.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not load transition effect curve!"));
	}

	levelTransition.timeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("TransitionTimeline"));
	timelineUpdateCallback.BindUFunction(this, FName{ TEXT("UpdateTimelineTransition") });

}

void APDP_3DGD_Group2GameMode::SetupRooms()
{
	bool roomsGeneratedCorrectly = rmg.GenerateRoomGraph(12);
	if (roomsGeneratedCorrectly)
	{
		rmg.GetSequence()->SpawnRooms(GetWorld());
	}
}
void APDP_3DGD_Group2GameMode::Tick(float delta)
{
	if (needsToStartFirstRoom)
	{
		SetupRooms();

		// turn on the first room teleporters
		ARoomBasicFloorAndWalls* room = Cast<ARoomBasicFloorAndWalls>(rmg.GetSequence()->GetCurrentRoom()->thisRoom);
		room->ActivateAsLevel();
	

		ReSpawnPlayer();
		needsToStartFirstRoom = false;
		SetActorTickEnabled(false);

		ReSpawnPlayer();
	}
	UE_LOG(LogTemp, Warning, TEXT("In game mode tick"));
}

void APDP_3DGD_Group2GameMode::RestartPlayer(AController* pControl)
{
	Super::RestartPlayer(pControl);
	playerController = pControl;

	// mini timeline setup step
	levelTransition.timeline->AddInterpFloat(levelTransition.floatCurveToUse, timelineUpdateCallback);
	UE_LOG(LogTemp, Display, TEXT("Added callback to timeline"));

	TArray<AActor*> foundCameras;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACameraActor::StaticClass(), foundCameras);
	UE_LOG(LogTemp, Display, TEXT("GameMode found %i cameras"), foundCameras.Num());
	
	// attempt to use the first
	worldCamera = Cast<ACameraActor>(foundCameras[0]);
	if (!worldCamera)
	{
		UE_LOG(LogTemp, Warning, TEXT("Error casting actor to camera"));
	}

	// get the navmesh 
	TArray<UObject*> navBoundsArray;
	GetObjectsOfClass(ANavMeshBoundsVolume::StaticClass(), navBoundsArray);
	if (navBoundsArray.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("No nav bounds"));
	}
	else
	{
		ANavMeshBoundsVolume* tryCast = Cast<ANavMeshBoundsVolume>(navBoundsArray[0]);
		if (!tryCast)
		{
			UE_LOG(LogTemp, Warning, TEXT("Could not convert object to nav mesh"));
		}
		else
		{
			UE_LOG(LogTemp, Display, TEXT("Found nav mesh"));
			navBounds = tryCast;
		}
	}
}

void APDP_3DGD_Group2GameMode::StartNextLevel()
{
	//for now turn on all teleporters at this point
	ARoomBasicFloorAndWalls* room = Cast<ARoomBasicFloorAndWalls>(rmg.GetSequence()->GetCurrentRoom()->thisRoom);
	if (room)
	{
		//room->GetTeleporterGroup()->TurnOnAllTeleporters();
		room->ActivateAsLevel();
	}	
	
	ReSpawnPlayer();
}
void APDP_3DGD_Group2GameMode::AllEnemiesDefeatedInLevel()
{
	Cast<ARoomBasicFloorAndWalls>(rmg.GetSequence()->GetCurrentRoom()->thisRoom)->WonLevel();
}
void APDP_3DGD_Group2GameMode::EndLevel()
{
	//clear up this level
	ARoomBasicFloorAndWalls* room = Cast<ARoomBasicFloorAndWalls>(rmg.GetSequence()->GetCurrentRoom()->thisRoom);
	if (room)
	{
		room->GetTeleporterGroup()->TurnOffAllTeleporters();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not cast room to ARoomBasic"));
	}

	// begin level transition
	levelTransition.startTransform = worldCamera->GetActorTransform();
	previousRoom = Cast<ARoomBasicFloorAndWalls>(rmg.GetSequence()->GetCurrentRoom()->thisRoom);

	rmg.GetSequence()->AdvanceSequence();
	if (rmg.GetSequence()->IsSequenceDone())
	{
		GameWon();
		UE_LOG(LogTemp, Warning, TEXT("GameWon triggered!"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Sequence not done"));
		// find where the camera needs to be
		FTransform cameraTransform;
		cameraTransform.SetRotation(rotationRelativeWorld.Quaternion());
		cameraTransform.SetLocation(positionRelativeRoom);

		const FTransform& roomTransform = rmg.GetSequence()->GetCurrentRoom()->thisRoom->GetTransform();
		cameraTransform = UKismetMathLibrary::ComposeTransforms(cameraTransform, roomTransform);

		FRotator cTformRot = cameraTransform.GetRotation().Rotator();
		FVector cTformLoc = cameraTransform.GetLocation();
		UE_LOG(LogTemp, Display, TEXT("Moving camera to %f %f %f, rotation %f %f %f"), cTformLoc.X, cTformLoc.Y, cTformLoc.Z, cTformRot.Roll, cTformRot.Pitch, cTformRot.Yaw);

		levelTransition.endTransform = cameraTransform;

		levelTransition.timeline->PlayFromStart();
		TransitionEffect();
	}
}

void APDP_3DGD_Group2GameMode::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("Beginning play in game mode"));

}

void APDP_3DGD_Group2GameMode::ReSpawnPlayer()
{
	AActor* playerPawn = playerController->GetPawn();
	if (!playerPawn)
	{
		UE_LOG(LogTemp, Fatal, TEXT("Could not retrieve player pawn : %i"), playerPawn);
		return;
	}

	const RoomSequence::RoomNode* const roomNodeToSpawnIn = rmg.GetSequence()->GetCurrentRoom();
	if (!roomNodeToSpawnIn && !roomNodeToSpawnIn->thisRoom)
	{
		UE_LOG(LogTemp, Fatal, TEXT("NO ROOMS TO SPAWN IN"));
		return;
	}

	ARoomBasicFloorAndWalls* roomToSpawnIn = Cast<ARoomBasicFloorAndWalls>(roomNodeToSpawnIn->thisRoom);
	if (!roomToSpawnIn)
	{
		UE_LOG(LogTemp, Fatal, TEXT("Error casting to room"));
		return;
	}

	const UPlayerSpawnGroupComponent* spawns = roomToSpawnIn->GetSpawnGroup();
	const UPlayerSpawnComponent* toSpawnAt = spawns->GetRandomSpawnPoint();
	if (!toSpawnAt)
	{
		//UE_LOG(LogTemp, Fatal, TEXT("No spawn point could be selected"));
		return;
	}
	else
	{
		FVector spawnLoc = toSpawnAt->GetComponentLocation();
		UE_LOG(LogTemp, Display, TEXT("Chose to spawn at point %f %f %f"), spawnLoc.X, spawnLoc.Y, spawnLoc.Z);
	}

	playerPawn->SetActorLocation(toSpawnAt->GetComponentLocation(), false, nullptr, ETeleportType::TeleportPhysics);
}

void APDP_3DGD_Group2GameMode::UpdateTimelineTransition(float val)
{
	if (val >= 1)
	{
		worldCamera->SetActorTransform(levelTransition.endTransform);
		levelTransition.timeline->Stop();
		StartNextLevel();
	}
	else
	{
		FTransform cameraTform = UKismetMathLibrary::TLerp(levelTransition.startTransform, levelTransition.endTransform, val);
		worldCamera->SetActorTransform(cameraTform);
	}
}
